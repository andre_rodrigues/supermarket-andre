# General dicount rule applied to any kind of product.
# 
# It can be configured with a DISCOUNT percentage that will
# be applied over the total amount if this amount is greater
# than a given LIMIT.
# 
# @returns a tuple containing two itens,
# the new total amount and the list of products.

# @example:
# 
# products = [
#   <Product price: 10.0>,
#   <Product price: 40.0>
# ]
# 
# rule = DiscountRules::DiscountOverLimit.new(30.0, 0.1)
# rule.run(50, products)
# 
# => [45.0, [<Product price: 10.0>, <Product price: 10.0>]]
module DiscountRules
  class DiscountOverLimit
    attr_reader :limit, :discount

    def initialize(limit, discount)
      @limit = limit
      @discount = discount
    end

    def run(total, products)
      new_total = if total > limit
        total * (1 - discount)
      else
        total
      end

      [new_total, products]
    end
  end
end
