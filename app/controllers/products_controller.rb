class ProductsController < ApplicationController
  def index
    @products = Product.all
  end

  def new
    @product = Product.new
  end

  def create
    @product = Product.new(permitted_params)

    if @product.save
      redirect_to products_path
    else
      render :new
    end
  end

  def destroy
    Product.find(params[:id]).destroy
    redirect_to products_path
  end

  private

  def permitted_params
    params.require(:product).permit(:name, :price)
  end
end
