# README

Checkout application that can scan items (Product) and apply certain promotional campaigns to give discounts.

## Requirements
* Ruby
* Bundler
* Postgresql
* Yarn

## Configuration

Basic setup and configuration
```
bundle exec bin/setup
yarn install
```

## Tests
```
bundle exec rspec
```

## Contributing
All discount rules can be found at `app/models/discount_rules`. They all follow the same interface
to make it easier to attach a sequence of rules in any process.
Each rule responds to the `run` method, which requires two arguments, a previous calculated total amount and
the list of products that are being analysed.

#### Example:
```rb
# A dicount rule that gives 30% off on a purchase
# over 100.00.
class MyDiscountRule
  def run(total, products)
    if total >= 100.00
      total = total * (1 - 0.3)
    end

    [total, products]
  end
end
```

Usage

```rb
expensive_product = Product.new(price: 100.00)

discount_rules = [MyDiscountRule.new]

checkout = Checkout.new(discount_rules)

checkout.scan(expensive_product)

checkout.total
=> 70.00
```
