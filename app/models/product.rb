class Product < ApplicationRecord
  validates :code, uniqueness: true

  before_validation :set_code

  def set_code
    self.code ||= Product.order(:code).last&.code.to_i.next
  end
end
