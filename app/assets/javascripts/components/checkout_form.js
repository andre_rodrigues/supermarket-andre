/** 
 * A checkout form that can dynamically add and remove items
 * along with a visualization of the grand total of a purchase.
 *
 * @example:
 *
 * <form class="checkout__form">
 *   <ul> <!-- Available Items -->
 *     <li class="checkout__select_product_button" product_id="1">Product 1</li>
 *     <li class="checkout__select_product_button" product_id="2">Product 2</li>
 *   </ul>
 *   
 *   <ul> <!-- Selected Items -->
 *     <li class="checkout__unselect_product_button" product_id="3">Product 3</li>
 *     <li class="checkout__unselect_product_button" product_id="4">Product 4</li>
 *   </ul>

 *   <span id="checkout__grand_total"> $100.00 </span>
 *
 *   <input type="submit" clas="checkout__submit" value="Finish checkout" />
 * </form>
 */ 
(function() {
  var ProductIdField = function(id) {
    var element = document.createElement("input");
      element.setAttribute("type", "hidden");
      element.setAttribute("name", "product_ids[]");
      element.value = id;

    return element;
  }

  var form = document.querySelector(".checkout__form");
  var selectButtons = form.querySelectorAll(".checkout__select_product_button");
  var unselectButtons = form.querySelectorAll(".checkout__unselect_product_button");
  var submit = form.querySelector(".checkout__submit");
  var grandTotal = form.querySelector("#checkout__grand_total");
  var snackbarContainer = form.querySelector('#toast');

  selectButtons.forEach(function(button) {
    button.addEventListener("click", function(e) {
      e.preventDefault();
      e.stopPropagation();

      var productId = button.getAttribute("product_id");
      form.append(new ProductIdField(productId));
      form.submit();
    });
  });

  unselectButtons.forEach(function(button) {
    button.addEventListener("click", function(e) {
      e.preventDefault();
      e.stopPropagation();

      var targetField = form.querySelector("#" + button.getAttribute("for"));
      targetField.remove();
      form.submit();
    });
  });

  submit.addEventListener("click", function(button) {
    if (snackbarContainer)
      snackbarContainer
        .MaterialSnackbar
        .showSnackbar({
          message: 'Finished checkout with ' + grandTotal.textContent,
          timeout: 2500
        });
  })
})();
