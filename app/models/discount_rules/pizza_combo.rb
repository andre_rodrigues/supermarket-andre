# Discount applied just for pizzas.
# 
# If there are 2 or more pizzas, the price of each pizza
# is fixed in 3.99
# 
# @returns a tuple containing two itens,
# the new total amount and the list of products.
# 
# @example:
# 
# products = [
#   <Product name: 'Pizza' price: 5.99>,
#   <Product name: 'Pizza' price: 5.99>
# ]
# 
# rule = DiscountRules::PizzaCombo.new
# rule.run(11.98, products)
# 
# => [7.98, [<Product name: 'Pizza' price: 3.99>, <Product name: 'Pizza' price: 3.99>]]
module DiscountRules
  class PizzaCombo
    MIN_PIZZAS = 2
    NEW_PRICE = 3.99

    def run(total, products)
      if products.count{ |p| pizza?(p) } >= MIN_PIZZAS
        products.each do |product|
          product.price = NEW_PRICE if pizza?(product)
        end

        total = products.map(&:price).reduce(:+)
      end

      [total, products]
    end

    private

    def pizza?(product)
      !!product.name[/pizza/i]
    end
  end
end
