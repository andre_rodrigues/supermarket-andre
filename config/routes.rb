Rails.application.routes.draw do
  root to: 'checkouts#show'

  resources :products
  resource :checkout, only: [:show, :update]
end
