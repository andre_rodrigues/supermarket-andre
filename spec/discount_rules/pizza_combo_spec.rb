require 'rails_helper'

RSpec.describe DiscountRules::PizzaCombo, type: :discount_rule do
  subject(:rule) do
    DiscountRules::PizzaCombo.new
  end

  describe '#run' do
    context 'when there are two or more pizzas' do
      let(:pizzas) do
        [
          products(:pizza),
          products(:pizza)
        ]
      end

      it 'applies the discount' do
        expect(rule.run(11.98, pizzas)).to eq [7.98, pizzas]
      end
    end

    context 'when there is less than two pizzas' do
      let(:pizzas) do
        [
          products(:pizza)
        ]
      end

      it 'does not apply the discount' do
        expect(rule.run(5.99, pizzas)).to eq [5.99, pizzas]
      end
    end
  end
end
