# Checkout class that can scan items (Product) and apply certain
# promotional campaigns to give discounts.
#
# @example:
#
# pizza = Product.find_by(name: 'Pizza')
#
# co = Checkout.new(DiscountRules::PizzaCombo.new)
# co.scan(pizza)
# co.scan(pizza)
# co.total
#
# => 7.98
class Checkout
  attr_reader :discount_rules

  def initialize(discount_rules)
    @discount_rules = Array(discount_rules)
    @original_products = []
  end

  def scan(product)
    @original_products << product
  end

  def total
    apply_discounts.first
  end

  def products
    apply_discounts.last
  end

  private

  # Internal: Iterate over all discount rules
  # applying each discount and storing the resulting total amount
  # and list of products with new prices.
  # 
  # @returns a tuple containing two itens,
  # the new total amount and the list of products.
  # 
  # @example:
  # 
  # => [7.98, [<Product name: 'Pizza' price: 3.99>, <Product name: 'Pizza' price: 3.99>]]
  def apply_discounts
    total_without_discount = @original_products.map(&:price).reduce(0, :+)

    discount_rules.reduce([total_without_discount, @original_products]) do |(total, products), rule|
      rule.run(total, products)
    end
  end
end
