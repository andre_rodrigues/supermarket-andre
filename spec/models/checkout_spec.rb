require 'rails_helper'

RSpec.describe Checkout, type: :model do
  let(:rules) do
    [
      DiscountRules::DiscountOverLimit.new(30, 0.1),
      DiscountRules::PizzaCombo.new
    ]
  end

  subject do
    described_class.new(rules)
  end

  describe '#scan' do
    let(:product) { products.sample }

    it 'stores and return the list of products' do
      expect(subject.scan(product)).to eq([product])
    end
  end

  describe '#total' do
    let(:items) { Product.all }

    before do
      items.each{ |item| subject.scan(item) }
    end

    it 'applies discounts and calculate the total amount' do
      expect(subject.total).to eq 29.646
    end
  end

  describe '#products' do
    let(:items) do
      [products(:pizza), products(:pizza)]
    end

    before do
      items.each{ |item| subject.scan(item) }
    end

    it 'returns the list of products with discounts applied' do
      items_with_discount = items.each{ |item| item.price = 3.99 }

      expect(subject.products).to eq(items_with_discount)
    end
  end
end
