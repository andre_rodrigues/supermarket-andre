require 'rails_helper'

RSpec.describe Product, type: :model do
  subject { products(:curry_sauce) }

  it 'has a name' do
    expect(subject.name).to eq 'Curry Sauce'
  end

  it 'has a price' do
    expect(subject.price).to eq 1.95
  end

  it 'has a unique code' do
    expect(subject.code).to eq 1

    new_product = Product.new(code: subject.code)
    expect(new_product).to be_invalid
    expect(new_product.errors[:code]).to include('has already been taken')
  end
end
