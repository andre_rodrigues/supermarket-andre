# Small seed to make it easier to test the application.
# -------------------------------------------
Product.create(code: 1, name: 'Curry Sauce', price: 1.95)
Product.create(code: 2, name: 'Pizza', price: 5.99)
Product.create(code: 3, name: 'Men T-Shirt', price: 25.00)
