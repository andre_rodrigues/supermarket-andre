class CheckoutsController < ApplicationController
  helper_method :available_products, :checkout

  def show
  end

  def update
    render :show
  end

  private

  def checkout
    return @checkout if @checkout.present?

    @checkout = Checkout.new(enabled_discount_rules)
    selected_product_ids.map do |product_id|
      product = Product.find(product_id)
      @checkout.scan(product)
    end

    @checkout
  end

  def available_products
    @available_products ||= Product.all
  end

  def selected_product_ids
    Array(params[:product_ids])
  end

  def enabled_discount_rules
    [
      DiscountRules::PizzaCombo.new,
      DiscountRules::DiscountOverLimit.new(30, 0.1)
    ]
  end
end
