require 'rails_helper'

RSpec.describe DiscountRules::DiscountOverLimit, type: :discount_rule do
  subject(:rule) do
    DiscountRules::DiscountOverLimit.new(5.0, 0.1)
  end

  it 'has a limit' do
    expect(rule.limit).to eq 5.0
  end

  it 'has a discount reference' do
    expect(rule.discount).to eq 0.1
  end

  describe '#run' do
    context 'when the total amount is greater than limit value' do
      let(:products) do
        [
          double(price: 2.0),
          double(price: 10.0)
        ]
      end

      it 'applies the discount' do
        expect(rule.run(12, products)).to eq [10.8, products]
      end
    end

    context 'when the total amount is lower than limit value' do
      let(:products) do
        [
          double(price: 2.0)
        ]
      end

      it 'does not apply the discount' do
        expect(rule.run(2, products)).to eq [2, products]
      end
    end
  end
end
